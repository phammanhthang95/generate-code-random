package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"
)

const charset = "abcdefghijklmnopqrstuvwxyz0123456789"

func RandomString(length int) string {
	return stringWithCharset(length, charset)
}

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func stringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}


func ArrayUnique(arr []string) []string {
	size := len(arr)
	result := make([]string, 0, size)
	temp := map[string]struct{}{}
	for i := 0; i < size; i++ {
		if _, ok := temp[arr[i]]; ok != true {
			temp[arr[i]] = struct{}{}
			result = append(result, arr[i])
		}
	}
	return result
}

func Include(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}

func main()  {
	listItemUnique := make([]string, 0, 500000)
	data := ""
	randomString := ""
	for i := 0; i < 1000000000; i++ {
		// lặp 1 tỉ lần
		randomString = RandomString(6)
		// mỗi lần lặp tạo 1 mã ngẫu nhiên dài 4
		if !Include(listItemUnique, randomString) {
			// nếu nó chưa tồn tại trong listItemUnique thì thêm mã đó vào listItemUnique
			listItemUnique = append(listItemUnique, randomString)
			data += randomString + "\n"
		}
		if len(listItemUnique) == 300 {
			// nếu check thấy nó đủ 500000 thì dừng vòng lặp lại và trả kết quả
			break
		}
		fmt.Println("gen total", len(listItemUnique))
	}
	// 36 kí tự
	// độ dài là 4
	// 500.000 mã

	// write json data to file
	err := writeToFile([]byte(data))
	if err != nil {
		log.Fatal(err)
	}
}

func writeToFile(data []byte) error {
	f, err := os.Create("code.txt")
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.Write(data)
	if err != nil {
		return err
	}
	return nil
}